function BulkPricingRule(bulkPrice = '39.90') {
  this.promoCode = 'promo0003';
  this.bulkPrice = bulkPrice;
}

BulkPricingRule.prototype.execute = async function (cart) {
  const products = cart.items.filter(i => i.product.promoCode === this.promoCode);
  const quantity = products.length;
  
  if (quantity <= 0) return 0;
  const listPrice = products[0].product.listPrice;
  
  const appliedPrice = quantity > 3 ? this.bulkPrice : listPrice;
  const subtotal = parseFloat(appliedPrice) * quantity;
  return subtotal;
}

module.exports = BulkPricingRule;