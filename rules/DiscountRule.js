function DiscountRule(promoCode = 'I<3AMAYSIM', discountPct = 10) {
  this.promoCode = promoCode;
  this.discountPct = discountPct;
}

DiscountRule.prototype.execute = async function (cart) {
  if (cart.promoCode !== this.promoCode) return 0;

  let discount = (cart.subtotal * (this.discountPct / 100)) * -1;
  return discount;
}

module.exports = DiscountRule;