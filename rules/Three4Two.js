function Three4TwoRule() {
  this.promoCode = 'promo0001';
}

Three4TwoRule.prototype.execute = async function (cart) {
  const products = cart.items.filter(i => i.product.promoCode === this.promoCode);
  const quantity = products.length;
  
  if (quantity <= 0 ) return 0;
  const listPrice = parseFloat(products[0].product.listPrice);

  let subtotal = 0;
  for (var i = 1; i <= quantity; i++) {
    if (i % 3 === 0) {
      continue;
    }
    subtotal += listPrice;
  }

  return subtotal;
}

module.exports = Three4TwoRule;