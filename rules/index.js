const Three4TwoRule = require('./Three4Two');
const BulkPricingRule = require('./BulkPricing');
const BundleRule = require('./Bundle');
const DefaultRule = require('./DefaultRule');
const DiscountRule = require('./DiscountRule');

module.exports = [
  new Three4TwoRule(),
  new BulkPricingRule(),
  new BundleRule(),
  new DefaultRule(),
  new DiscountRule(),
];