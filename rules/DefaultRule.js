function DefaultRule() {
}

DefaultRule.prototype.execute = async function (cart) {
  const nonPromoItems = cart.items.filter(i => {
    return !i.product.promoCode && !i.isFree
  });
  const quantity = nonPromoItems.length;

  let subtotal = 0;
  for(var i = 0; i < quantity; i++) {
    const listPrice = nonPromoItems[i].product.listPrice;
    subtotal += parseFloat(listPrice);
  }

  return subtotal;
}

module.exports = DefaultRule;