function BundleRule(bundledItemCode = '1gb') {
  this.promoCode = 'promo0002';
  this.bundledItemCode = bundledItemCode;
}

BundleRule.prototype.execute = async function (cart) {
  const products = cart.items.filter(i => i.product.promoCode === this.promoCode);
  const quantity = products.length;

  if (quantity <= 0) return 0;
  const listPrice = products[0].product.listPrice;

  for(var i = 0; i < quantity; i++) {
    if (!products[i].isCalculated) {
      cart.add(this.bundledItemCode, null, true);
      products[i].isCalculated = true;
    }
  }

  return parseFloat(listPrice) * quantity;
}

module.exports = BundleRule;