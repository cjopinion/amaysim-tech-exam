# Requirements to run app:
1. node 8.11

# Startup instructions
- clone repository
- run `npm install`
- run `npm start`

# Execute tests
- run `npm test`

# Amaysim Shopping Cart Docs #
- Access simple UI from `http://localhost:6100` via your favorite browser! :)

### Endpoints ###
1. Add items to cart via `http://localhost:6100/cart` GET or POST endpoint
```
POST body structure
{
  "productCode": "ult_small",
  "promoCode": "I<3AMAYSIM"
}
```
```
GET params
http://localhost:6100/cart?productCode=ult_small&promoCode=I<3AMAYSIM
```

2. GET endpoint to retrieve current cart items & total: `http://localhost:6100/cart/total`
2. GET endpoint to retrieve reset current cart: `http://localhost:6100/cart/reset`
3. GET endpoint to retrieve  current product catalogue: `http://localhost:6100/products`