function errorpage({request, response, statusCode = 500, errMsg = 'Something went wrong'}) {
  response.writeHead(statusCode, {'Content-Type': 'text/html'});
  response.end(`<h1>${errMsg}</h1>`);
}

module.exports = errorpage;