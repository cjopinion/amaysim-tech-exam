const ShoppingCart = require('./ShoppingCart');
const rules = require('./rules');

let cart = new ShoppingCart(rules);
async function addToCartHandler({request, response}) {
  const req = request.method === 'GET' ? request.query : request.body;
  const productCode = req.productCode;
  const promoCode = req.promoCode;
  
  await cart.add(productCode, promoCode);
  await cart.calculate();

  const res = {
    items: cart.items,
  };
  
  response.writeHead(200, {'Content-Type': 'application/json'});
  response.write(JSON.stringify(res));
  response.end();
}

function CartDisplay({productCode, productName, quantity = 0}) {
  this.productCode = productCode;
  this.productName = productName;
  this.quantity = quantity;
}

function getCart() {
  const items = [];
  cart.items.forEach((cartItem) => {
    let itemDisplay = items.find((i) => i.productCode === cartItem.productCode);
    if (!itemDisplay) {
      itemDisplay = new CartDisplay({productCode: cartItem.productCode, productName: cartItem.product.name}); 
      items.push(itemDisplay);
    }
    itemDisplay.quantity++;
  });

  const res = {
    items,
    total: cart.total
  };

  return res;
}

function getCartHandler({request, response}) {
  const res = getCart();
  response.writeHead(200, {'Content-Type': 'application/json'});
  response.write(JSON.stringify(res));
  response.end();
}

function resetCartHandler({request, response}) {
  cart = new ShoppingCart(rules);
  response.writeHead(200, {'Content-Type': 'text/plain'});
  response.end('Shopping cart cleared.');
}

module.exports.addToCartHandler = addToCartHandler;
module.exports.getCartHandler = getCartHandler;
module.exports.resetCartHandler = resetCartHandler;
module.exports.getCart = getCart;