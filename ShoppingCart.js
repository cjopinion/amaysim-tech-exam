const { products } = require('./products');
const rules = require('./rules');

function ShoppingCart(pricingRules = rules) {
  this.items = [];
  this.total = '0.00';
  this.subtotal = 0;
  this.promoCode = '';
  this.pricingRules = pricingRules;
}

function CartItem({productCode, product, isFree = false}) {
  this.productCode = productCode;
  this.product = product;
  this.isFree = isFree;
  this.isCalculated = false;
}

async function add(productCode, promoCode, isFree = false) {
  await Promise.all([this.addItem(productCode, isFree), this.applyPromoCode(promoCode)]);
}

async function addItem(productCode, isFree) {
  if (!productCode) return;
  const item = new CartItem({productCode, product: products[productCode], isFree});
  this.items.push(item);
}

async function applyPromoCode(promoCode) {
  if (!promoCode) return;
  this.promoCode = promoCode;
}

async function calculate() {
  this.subtotal = 0;
  
  for (const rule of this.pricingRules) {
    this.subtotal += await rule.execute(this);
  }
  
  this.total = (this.subtotal).toFixed(2);
}

ShoppingCart.prototype.add = add;
ShoppingCart.prototype.calculate = calculate;
ShoppingCart.prototype.addItem = addItem;
ShoppingCart.prototype.applyPromoCode = applyPromoCode;

module.exports = ShoppingCart;