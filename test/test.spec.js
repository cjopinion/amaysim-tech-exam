const { expect } = require('chai');
const ShoppingCart = require('../ShoppingCart');
const rules = require('../rules');

describe('Amaysim Tests', () => {
  let cart;
  it('Scenario 1', async () => {
    cart = new ShoppingCart(rules);
    await cart.add('ult_small').then(() => cart.calculate());
    await cart.add('ult_small').then(() => cart.calculate());
    await cart.add('ult_small').then(() => cart.calculate());
    await cart.add('ult_large').then(() => cart.calculate());

    expect(cart.total).to.equal('94.70');
  });

  it('Scenario 2', async () => {
    cart = new ShoppingCart(rules);
    await cart.add('ult_small').then(() => cart.calculate());
    await cart.add('ult_small').then(() => cart.calculate());

    await cart.add('ult_large').then(() => cart.calculate());
    await cart.add('ult_large').then(() => cart.calculate());
    await cart.add('ult_large').then(() => cart.calculate());
    await cart.add('ult_large').then(() => cart.calculate());

    expect(cart.total).to.equal('209.40');
  });

  it('Scenario 3', async () => {
    cart = new ShoppingCart(rules);
    await cart.add('ult_small').then(() => cart.calculate());
    await cart.add('ult_medium').then(() => cart.calculate());
    await cart.add('ult_medium').then(() => cart.calculate());

    expect(cart.total).to.equal('84.70');

    const freeBundle = cart.items.filter(i => i.productCode === '1gb' && i.isFree);
    expect(freeBundle).to.not.be.undefined;
    expect(freeBundle.length).to.equal(2);
  });

  it('Scenario 3.1', async () => {
    cart = new ShoppingCart(rules);
    await cart.add('ult_small').then(() => cart.calculate());
    await cart.add('ult_medium').then(() => cart.calculate());
    await cart.add('ult_medium').then(() => cart.calculate());
    await cart.add('1gb').then(() => cart.calculate());

    expect(cart.total).to.equal('94.60');

    const freeBundle = cart.items.filter(i => i.productCode === '1gb' && i.isFree);
    expect(freeBundle).to.not.be.undefined;
    expect(freeBundle.length).to.equal(2);
  });

  it('Scenario 4', async () => {
    cart = new ShoppingCart(rules);
    await cart.add('ult_small').then(() => cart.calculate());
    await cart.add('1gb', 'I<3AMAYSIM').then(() => cart.calculate());
    
    expect(cart.total).to.equal('31.32');
  });
});