function Product({name = '', listPrice = '0.00', promoCode = null}) {
  this.name = name;
  this.listPrice = listPrice;
  this.promoCode = promoCode;
};

const products = {};

function addNewProduct({code, name, listPrice, promoCode}) {
  products[code] = new Product({name, listPrice, promoCode});
}

addNewProduct({code: 'ult_small', name: 'Unlimited 1GB', listPrice: '24.90', promoCode: 'promo0001'});
addNewProduct({code: 'ult_medium', name: 'Unlimited 2GB', listPrice: '29.90', promoCode: 'promo0002'});
addNewProduct({code: 'ult_large', name: 'Unlimited 5GB', listPrice: '44.90', promoCode: 'promo0003'});
addNewProduct({code: '1gb', name: '1GB Data-pack', listPrice: '9.90'});

function getProductsHandler({request, response}) {
  response.writeHead(200, {'Content-Type': 'application/json'});
  response.write(JSON.stringify(products));
  response.end();
}

module.exports.getProductsHandler = getProductsHandler;
module.exports.products = products;