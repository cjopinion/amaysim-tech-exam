const http = require('http');
const url = require('url');

const { homepage, viewCart } = require('./homepage');
const { getProductsHandler } = require('./products');
const { addToCartHandler, getCartHandler, resetCartHandler } = require('./cart');
const error = require('./error');

const routes = [
  {path: '/', method: 'GET', handler: homepage},
  {path: '/products', method: 'GET', handler: getProductsHandler},
  {path: '/cart', method: 'POST', handler: addToCartHandler},
  {path: '/cart', method: 'GET', handler: addToCartHandler},
  {path: '/cart/total', method: 'GET', handler: getCartHandler},
  {path: '/cart/view', method: 'GET', handler: viewCart},
  {path: '/cart/reset', method: 'GET', handler: resetCartHandler},
  {path: '/error', method: 'GET', handler: error},
];

/**
 * TODO
 * 1. endpoint to upsert products in product catalogue
 * 2. endpoint to upsert rules --- array of rules w ID
 * 3. store carts, get carts by ID
 */

const server  = http.createServer((request, response) => {
  let data = [];
  
  request.on('data', chunk => {
    data.push(chunk)
  });
  request.on('end', async () => {
    const urlParts = url.parse(request.url, true);
    request.query = urlParts.query;
    request.path = urlParts.pathname;

    try {
      request.body = JSON.parse(data);
    } catch (err) {
      request.body = {};
    }

    const route = routes.find((r) => {
      return r.path === request.path && r.method === request.method;
    });
  
    if (route) {
      await route.handler({request, response});
    } else {
      error({request, response, statusCode: 401, errMsg: 'Page Not Found'});
    }
  });
});

server.listen(6100, () => {
  console.log('Server started... Listening on port 6100...');
});