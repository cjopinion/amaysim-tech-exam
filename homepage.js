const fs = require('fs');
const http = require('http');
const { promisify } = require('util');
const readFile = promisify(fs.readFile);

const error = require('./error');
const { getCart } = require('./cart');

function renderHtml({request, response, path}) {
  fs.readFile(path, (err, data) => {
    if (err) {
      error({request, response});
    }

    response.writeHead(200, {'Content-Type': 'text/html'});
    response.write(data);
    response.end();
  });
}

function homepage({request, response}) {
  const path = 'web/home.html';
  renderHtml({request, response, path});
}

function viewCart({request, response}) {
  return new Promise((resolve, reject) => {
    const cart = getCart();
    resolve(cart);
  })
  .then(cart => {
    let list = '';
    cart.items.forEach(item => {
      list += `<li class="list-group-item">${item.productName} x ${item.quantity}</list>`;
    });

    if (!list) {
      list += 'No items in your cart.';
    }
    return {list, cart};
  })
  .then(async ({list, cart}) => {
    let data = await readFile('web/view_cart.html', { encoding: 'utf-8' });
    return {data, list, cart};
  })
  .then(({data, list, cart}) => {
    data = data.replace(/{{cartList}}/, list);
    return {data, cart};
  })
  .then(({data, cart}) => {
    data = data.replace(/{{cartTotal}}/, cart.total);
    return data;
  })
  .then((data) => {
    response.writeHead(200, {'Content-Type': 'text/html'});
    response.write(data);
    response.end();

  })
  .catch((err) => {
    error({request, response, errMsg: err});
  });
}

module.exports.viewCart = viewCart;
module.exports.homepage = homepage;